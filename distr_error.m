%% Mixture of zero mean Gaussian probability distribution


eps = 0.5;

% rho
sigma1_rho = (1.518* 10^(-5))^2;
mean_rho = [0;0];
sigma_rho= cat(3,sigma1_rho,5*sigma1_rho);
p_rho = [1-eps, eps];
obj_rho = gmdistribution(mean_rho,sigma_rho,p_rho);

rho_noise = random(obj_rho,fix(T))  ;

% theta
sigma1_theta = (2.787 * (pi/180) * 10^(-3))^2;
mean_theta = [0;0];
sigma_theta = cat(3,sigma1_theta,5*sigma1_theta);
p_theta = [1-eps, eps];
obj_theta = gmdistribution(mean_theta,sigma_theta,p_theta);

theta_noise = random(obj_theta,fix(T));

% phi
sigma1_phi = (1.404* (pi/180) * 10^(-3))^2;
mean_phi = [0;0];
sigma_phi = cat(3,sigma1_phi,5*sigma1_phi);
p_phi = [1-eps, eps];
obj_phi = gmdistribution(mean_phi,sigma_phi,p_phi);

phi_noise = random(obj_phi,fix(T)) ;