function Fj = jacobian_NERM(Xt,X, ht_eci, mu)

x = X(1);
y = X(2);
z = X(3);

r = norm(Xt(1:3));


% velocity of target
r_dot = norm(Xt(4:6));

% angular moment of target
h = norm(ht_eci);

% derivative of true anomaly
f_dot = h/r^2;




Fj = [0, 0, 0, 1, 0, 0;
 0, 0, 0, 0, 1, 0;
 0, 0, 0, 0, 0, 1;
 f_dot^2 - mu/((r + x)^2 + y^2 + z^2)^(3/2) + (3*mu*(2*r + 2*x)*(r + x))/(2*((r + x)^2 + y^2 + z^2)^(5/2)),...
 (3*mu*y*(r + x))/((r + x)^2 + y^2 + z^2)^(5/2) - (2*f_dot*r_dot)/r, (3*mu*z*(r + x))/((r + x)^2 + y^2 + z^2)^(5/2),...
 0, 2*f_dot, 0;
 f_dot^2 + (2*f_dot*r_dot)/r + (3*mu*y*(2*r + 2*x))/(2*((r + x)^2 + y^2 + z^2)^(5/2)),...
 (3*mu*y^2)/((r + x)^2 + y^2 + z^2)^(5/2) - mu/((r + x)^2 + y^2 + z^2)^(3/2),...
 (3*mu*y*z)/((r + x)^2 + y^2 + z^2)^(5/2), -2*f_dot, 0, 0;
 (3*mu*z*(2*r + 2*x))/(2*((r + x)^2 + y^2 + z^2)^(5/2)), (3*mu*y*z)/((r + x)^2 + y^2 + z^2)^(5/2),...
 (3*mu*z^2)/((r + x)^2 + y^2 + z^2)^(5/2) - mu/((r + x)^2 + y^2 + z^2)^(3/2), 0, 0, 0];


end