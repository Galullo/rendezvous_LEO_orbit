%earth plot

grs80 = referenceEllipsoid('grs80','km');
domeRadius =  3000;  % km
domeLat =  39;       % degrees
domeLon = -77;       % degrees
domeAlt = 0;         % km

[x,y,z] = sphere(20);
xEast  = domeRadius * x;
yNorth = domeRadius * y;
zUp    = domeRadius * z;
zUp(zUp < 0) = 0;
% figure('Renderer','opengl')
% surf(xEast, yNorth, zUp,'FaceColor','yellow','FaceAlpha',0.5)
% axis equal

[xECEF, yECEF, zECEF] ...
    = enu2ecef(xEast, yNorth, zUp, domeLat, domeLon, domeAlt, grs80);
% surf(xECEF, yECEF, zECEF,'FaceColor','yellow','FaceAlpha',0.5)
% axis equal

figure('Renderer','opengl')
ax = axesm('globe','Geoid',grs80,'Grid','on', ...
    'GLineWidth',1,'GLineStyle','-',...
    'Gcolor',[0.9 0.9 0.1],'Galtitude',100);
ax.Position = [0 0 1 1];
axis equal off
view(3)

load topo
geoshow(topo,topolegend,'DisplayType','texturemap')
demcmap(topo)
land = shaperead('landareas','UseGeoCoords',true);
plotm([land.Lat],[land.Lon],'Color','black')
rivers = shaperead('worldrivers','UseGeoCoords',true);
plotm([rivers.Lat],[rivers.Lon],'Color','blue')