function Y = cartesian2spherical(X)

x   = X(1);
y   = X(2);
z   = X(3);
vx  = X(4);
vy  = X(5);
vz  = X(6);



rho         = sqrt(x^2 + y^2 + z^2);

% define atan for 2*pi
th = atan2(y,x);
%th = atan(y/x);

% if(th < 0)
%     theta = th + 2*pi;
% else
      theta = th;
% end

%ph         = atan(z/sqrt(x^2 + y^2));

ph         = atan2(z, sqrt(x^2 + y^2));
 

% if(ph < 0)
%     phi = ph + 2*pi;
% else
    phi = ph;
% end



rho_dot     = (x*vx + y*vy + z*vz)/rho;
theta_dot   = (vy*x - vx*y)/(x^2 + y^2);
phi_dot     = (vz*(x^2 + y^2) - z*(x*vx + y*vy))/(sqrt(x^2 + y^2)*rho^2);

% if(X == zeros(6,1))
%     Y = zeros(6,1);
% else
     Y = [rho; theta; phi; rho_dot; theta_dot; phi_dot];
%end

end


% % define atan for 2*pi
% if (x>0 && y>= 0)
%     theta       = atan(y/x) + pi;
% elseif ( x<0 && y<0)
%     theta       = atan(y/x) - pi;
% else
%     theta       = atan(y/x);
