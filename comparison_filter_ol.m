% Comparison filter

time_s = 0:Tsens:t_sim;

pl_state = @plot_states;
pl_error_state = @plot_error_states;

for choose_orbit = 1:2
    
    switch(choose_orbit)
        case 1
            orbit_name = 'Low_Earth_Orbit';
            %...Input data LEO:
            %  Prescribed initial orbital parameters of target A:
            rp    = rT + r_LEO;         % km
            e     = 0.0005100;
            i     = 51.6391;            % deg
            RA    = 270.8069;           % deg
            omega = 116.0974;           % deg
            theta = 0;
            
            %  Additional computed parameters:
            ra = rp*(1 + e)/(1 - e);
            h  = sqrt(2*mu*rp*ra/(ra + rp));   %(km^2/)s
            a  = (rp + ra)/2;
            T  = 2*pi/sqrt(mu)*a^1.5;
            n  = 2*pi/T;
            
            % Error's model
            distr_error;
        case 2
            orbit_name = 'Proba3_Orbit';
            %...Input data Orbit Molnija:
            %  Prescribed initial orbital parameters of target A:
            rp    =  rT + rp_e;             % km
            ra    =  rT + ra_e;             % km
            i     =  59;                    % deg
            RA    =  84;                    % deg
            omega =  188;                   % deg
            theta = 0;                      % deg
            
            %  Additional computed parameters:
            e = (ra - rp)/(rp + ra);
            h  = sqrt(2*mu*rp*ra/(ra + rp));   %(km^2/)s
            a  = (rp + ra)/2;
            T  = 2*pi/sqrt(mu)*a^1.5;
            n  = 2*pi/T;
     
            % Error's model
            distr_error;
    end
    
            
    sim('complete_navigation', t_sim)
    warning('off')
    
    % conversion of relative motion in spherical coordiantes
    S_relative_motion_LVLH = zeros(length(relative_motion_LVLH), 6);
    for i = 1:length(relative_motion_LVLH)
        S_relative_motion_LVLH(i,:) = cartesian2spherical(relative_motion_LVLH(i,:))';
    end
    
    S_dsdre_estimated = zeros(length(X_dsdre_estimated), 6);
    for i = 1:length(X_dsdre_estimated)
        S_dsdre_estimated(i,:) = cartesian2spherical(X_dsdre_estimated(i,:))';
    end
    
    S_kalman_estimated = zeros(length(X_kalman_estimated), 6);
    for i = 1:length(X_dsdre_estimated)
        S_kalman_estimated(i,:) = cartesian2spherical(X_kalman_estimated(i,:))';
    end
    S_huber_estimated = zeros(length(X_huber_estimated), 6);
    for i = 1:length(X_dsdre_estimated)
        S_huber_estimated(i,:) = cartesian2spherical(X_huber_estimated(i,:))';
    end
    
    
    
    % plot state estimation and real state
    folder = ['report_figure/ol/P001/' orbit_name '/figure'];
    mkdir(folder) 
    pl_state(time, time_s, measurements, X_dsdre_estimated, X_kalman_estimated, X_huber_estimated, relative_motion_LVLH, 'Cartesian', folder)
    pl_state(time, time_s, S_measurements, S_dsdre_estimated, S_kalman_estimated, S_huber_estimated, S_relative_motion_LVLH, 'Spherical', folder)
   
    % plot state estimation and real state
    folder = ['report_figure/ol/P001/' orbit_name '/error'];
    mkdir(folder)
    pl_error_state(time, (relative_motion_LVLH-X_dsdre_estimated), (relative_motion_LVLH-X_kalman_estimated), (relative_motion_LVLH-X_huber_estimated), 'Cartesian', folder)
    pl_error_state(time, (S_relative_motion_LVLH-S_dsdre_estimated), (S_relative_motion_LVLH-S_kalman_estimated), (S_relative_motion_LVLH-S_huber_estimated), 'Spherical', folder)

    save([folder '/../state_data.mat'], 'X_dsdre_estimated', 'X_kalman_estimated', 'X_huber_estimated','S_dsdre_estimated', 'S_kalman_estimated', 'S_huber_estimated', 'relative_motion_LVLH', 'S_relative_motion_LVLH');
    save([folder '/../init_cond.mat'], 'X0_ch_f', 'X0_ch', 'S0_ch_f', 'delta_S0_ch_f')
end



function plot_states(t, ts, s_sens, sd, sk, sh, tr_st, coordinates, folder)

% string for axis label
cart = [string('x [km]'), string('y [km]'),string('z [km]'),string('v_x [km/s]'), string('v_y [km/s]'), string('v_z [km/s]')];
sphe = [string('\rho [km]'), string('\theta [deg]'), string('\Phi [deg]'), string('\rho^{\prime} [km/s]'), string('\theta^{\prime} [deg/s]'), string('\Phi^{\prime} [deg/s]')];

% choose the correct frame
if(strcmp(coordinates, 'Cartesian') == 1)
    coord = cart;
    rd = 1;     % from rad to deg 
elseif(strcmp(coordinates, 'Spherical') == 1)
    coord = sphe;
    rd = 180/pi;     % from rad to deg
end

% color of plots
col = [string('k.'), string('c'), string('r'), string('b'), string('k')];


figure,
subplot(3,1,1)
plot(ts, s_sens(:,1), char(col(1)), 'MarkerSize',12), hold on,
plot(t, tr_st(:,1), char(col(5)),'LineWidth',2), hold on
plot(t, sd(:,1), char(col(2)),'LineWidth',1.5), hold on,
plot(t, sk(:,1), char(col(3)),'LineWidth',1.5), hold on,
plot(t, sh(:,1), char(col(4)),'LineWidth',1.5), hold on,
xlabel('time [s]'), ylabel(coord(1)), legend('measurements', 'real','DSDRE', 'Kalman', 'Huber')

subplot(3,1,2)
plot(ts, rd*s_sens(:,2), char(col(1)),'MarkerSize',12), hold on,
plot(t, rd*tr_st(:,2), char(col(5)),'LineWidth',2), hold on
plot(t, rd*sd(:,2), char(col(2)),'LineWidth',1.5),hold on,
plot(t, rd*sk(:,2), char(col(3)),'LineWidth',1.5), hold on,
plot(t, rd*sh(:,2), char(col(4)),'LineWidth',1.5), hold on,
xlabel('time [s]'), ylabel(coord(2)), legend('measurements', 'real','DSDRE', 'Kalman', 'Huber')

subplot(3,1,3)
plot(ts, rd*s_sens(:,3), char(col(1)),'MarkerSize',12), hold on,
plot(t, rd*tr_st(:,3), char(col(5)),'LineWidth',2), hold on
plot(t, rd*sd(:,3), char(col(2)),'LineWidth',1.5),hold on,
plot(t, rd*sk(:,3), char(col(3)),'LineWidth',1.5), hold on,
plot(t, rd*sh(:,3), char(col(4)),'LineWidth',1.5), hold on,
xlabel('time [s]'), ylabel(coord(3)), legend('measurements', 'real','DSDRE', 'Kalman', 'Huber')

savefig([folder '/' coordinates '_coordianates_position.fig'])
saveas(gcf,[folder '/' coordinates '_coordianates_position.eps'],'epsc')

% plot velocity
figure,
subplot(3,1,1)
plot(t, tr_st(:,4), char(col(5)),'LineWidth',2),hold on,
plot(t, sd(:,4), char(col(2)),'LineWidth',1.5),hold on,
plot(t, sk(:,4), char(col(3)),'LineWidth',1.5), hold on,
plot(t, sh(:,4), char(col(4)),'LineWidth',1.5), hold on,
xlabel('time [s]'), ylabel(coord(4)), legend('real','DSDRE', 'Kalman', 'Huber')    

subplot(3,1,2)
plot(t, rd*tr_st(:,5), char(col(5)),'LineWidth',2),hold on,
plot(t, rd*sd(:,5), char(col(2)),'LineWidth',1.5),hold on,
plot(t, rd*sk(:,5), char(col(3)),'LineWidth',1.5), hold on,
plot(t, rd*sh(:,5), char(col(4)),'LineWidth',1.5), hold on,
xlabel('time [s]'), ylabel(coord(5)), legend('real','DSDRE', 'Kalman', 'Huber')

subplot(3,1,3)
plot(t, rd*tr_st(:,6), char(col(5)),'LineWidth',2),hold on,
plot(t, rd*sd(:,6), char(col(2)),'LineWidth',1.5),hold on,
plot(t, rd*sk(:,6), char(col(3)),'LineWidth',1.5), hold on,
plot(t, rd*sh(:,6), char(col(4)),'LineWidth',1.5), hold on,
xlabel('time [s]'), ylabel(coord(6)), legend('real','DSDRE', 'Kalman', 'Huber')

savefig([folder '/' coordinates '_coordianates_velocity.fig'])
saveas(gcf,[folder '/' coordinates '_coordianates_velocity.eps'],'epsc')

end


function plot_error_states(t, ed, ek, eh, coordinates, folder)

% string for axis label
cart = [string('x [km]'), string('y [km]'),string('z [km]'),string('v_x [km/s]'), string('v_y [km/s]'), string('v_z [km/s]')];
sphe = [string('\rho [km]'), string('\theta [deg]'), string('\Phi [deg]'), string('\rho^{\prime} [km/s]'), string('\theta^{\prime} [deg/s]'), string('\Phi^{\prime} [deg/s]')];

% choose the correct frame
if(strcmp(coordinates, 'Cartesian') == 1)
    coord = cart;
    rd = 1;     % from rad to deg 
elseif(strcmp(coordinates, 'Spherical') == 1)
    coord = sphe;
    rd = 180/pi;     % from rad to deg
end

% color of plots
col = [string('k.'), string('c'), string('r'), string('b'), string('k')];


figure,
subplot(3,1,1)
plot(t, ed(:,1), char(col(2)),'LineWidth',1.5), hold on,
plot(t, ek(:,1), char(col(3)),'LineWidth',1.5), hold on,
plot(t, eh(:,1), char(col(4)),'LineWidth',1.5), hold on,
xlabel('time [s]'), ylabel(coord(1)), legend('DSDRE', 'Kalman', 'Huber')

subplot(3,1,2)
plot(t, rd*ed(:,2), char(col(2)),'LineWidth',1.5),hold on,
plot(t, rd*ek(:,2), char(col(3)),'LineWidth',1.5), hold on,
plot(t, rd*eh(:,2), char(col(4)),'LineWidth',1.5), hold on,
xlabel('time [s]'), ylabel(coord(2)), legend('DSDRE', 'Kalman', 'Huber')

subplot(3,1,3)
plot(t, rd*ed(:,3), char(col(2)),'LineWidth',1.5),hold on,
plot(t, rd*ek(:,3), char(col(3)),'LineWidth',1.5), hold on,
plot(t, rd*eh(:,3), char(col(4)),'LineWidth',1.5), hold on,
xlabel('time [s]'), ylabel(coord(3)), legend('DSDRE', 'Kalman', 'Huber')

savefig([folder '/' coordinates '_coordianates_position_error.fig'])
saveas(gcf,[folder '/' coordinates '_coordianates_position_error.eps'],'epsc')

% plot velocity
figure,
subplot(3,1,1)
plot(t, ed(:,4), char(col(2)),'LineWidth',1.5),hold on,
plot(t, ek(:,4), char(col(3)),'LineWidth',1.5), hold on,
plot(t, eh(:,4), char(col(4)),'LineWidth',1.5), hold on,
xlabel('time [s]'), ylabel(coord(4)), legend('DSDRE', 'Kalman', 'Huber')

subplot(3,1,2)
plot(t, rd*ed(:,5), char(col(2)),'LineWidth',1.5),hold on,
plot(t, rd*ek(:,5), char(col(3)),'LineWidth',1.5), hold on,
plot(t, rd*eh(:,5), char(col(4)),'LineWidth',1.5), hold on,
xlabel('time [s]'), ylabel(coord(5)), legend('DSDRE', 'Kalman', 'Huber')

subplot(3,1,3)
plot(t, rd*ed(:,6), char(col(2)),'LineWidth',1.5),hold on,
plot(t, rd*ek(:,6), char(col(3)),'LineWidth',1.5), hold on,
plot(t, rd*eh(:,6), char(col(4)),'LineWidth',1.5), hold on,
xlabel('time [s]'), ylabel(coord(6)), legend('DSDRE', 'Kalman', 'Huber')

savefig([folder '/' coordinates '_coordianates_velocity_error.fig'])
saveas(gcf,[folder '/' coordinates '_coordianates_velocity_error.eps'],'epsc')


end
