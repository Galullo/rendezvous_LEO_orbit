function Y = NERM(X, r, r_dot, f_dot, mu)


x = X(1);
y = X(2);
z = X(3);
x_dot = X(4);
y_dot = X(5);
z_dot = X(6);




rd = sqrt((r + x)^2 + y^2 + z^2);


x_dot_dot = 2*f_dot*(y_dot - (r_dot/r)*y) + (f_dot^2)*x + mu/(r^2) - (mu/(rd^3))*(r+x);
y_dot_dot = -2*f_dot*(x_dot - (r_dot/r)*x) + (f_dot^2)*x - (mu/(rd^3))*y;
z_dot_dot = -(mu/(rd^3))*z;

Y = [x_dot; y_dot; z_dot; x_dot_dot; y_dot_dot; z_dot_dot];

end
