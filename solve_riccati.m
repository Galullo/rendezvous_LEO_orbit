function [P] = solve_riccati(Am, B0, Qm, Rm)

% resolve Riccati Algebric Equation (dimension of state is 4)
H = [Am, -B0*pinv(Rm)*B0'; -Qm, -Am'];

n = length(H);

% Use QZ factorization 
[T, D] = eig(H,eye(n),'qz','vector');

[Ds, I] = sort(real(D));

U = T(:,I(1:n/2));
Px = U(n/2 +1 : n,:)*pinv(U(1:n/2,:));

% symmetric solution
P = real((Px + Px'))/2;

end
