function X = spherical2cartesian(Y)

rho         = Y(1);
theta       = Y(2);
phi         = Y(3);
rho_dot     = Y(4);
theta_dot   = Y(5);
phi_dot     = Y(6);


x = rho*cos(phi)*cos(theta);
y = rho*cos(phi)*sin(theta);
z = rho*sin(phi);

vx = rho_dot*cos(phi)*cos(theta) - rho*sin(phi)*cos(theta)*phi_dot - rho*cos(phi)*sin(theta)*theta_dot;
vy = rho_dot*cos(phi)*sin(theta) - rho*sin(phi)*sin(theta)*phi_dot + rho*cos(phi)*cos(theta)*theta_dot;
vz = rho_dot*sin(phi) + rho*cos(phi)*phi_dot;

X = [x; y; z; vx; vy; vz];
end