% figure prova

figure,
subplot(3,1,1), 
plot(time, relative_motion_LVLH(:,1),'k','LineWidth',1.5), hold on,
plot(time, X_dsdre_estimated(:,1),'c','LineWidth',1.5), hold on,
plot(time, X_kalman_estimated(:,1),'r','LineWidth',1.5), hold on,
plot(time, X_Huber_estimated(:,1),'b','LineWidth',1.5),
xlabel('[time] = s'), ylabel('[x] = km'), grid on
subplot(3,1,2), plot(time, relative_motion_LVLH(:,2),'k','LineWidth',1.5),hold on,
plot(time, X_dsdre_estimated(:,2),'c','LineWidth',1.5), hold on, 
plot(time, X_kalman_estimated(:,2),'r','LineWidth',1.5), hold on,
plot(time, X_Huber_estimated(:,2),'b','LineWidth',1.5), hold on,
xlabel('[time] = s'), ylabel('[y] = km'), grid on
subplot(3,1,3), plot(time, relative_motion_LVLH(:,3),'k','LineWidth',1.5), hold on,
plot(time, X_dsdre_estimated(:,3),'c','LineWidth',1.5), hold on,
plot(time, X_kalman_estimated(:,3),'r','LineWidth',1.5), hold on, 
plot(time, X_Huber_estimated(:,3),'b','LineWidth',1.5),
xlabel('[time] = s'), ylabel('[z] = km'), grid on

figure,
subplot(3,1,1), plot(time, relative_motion_LVLH(:,4),'k','LineWidth',1.5), hold on,
plot(time, X_dsdre_estimated(:,4),'c','LineWidth',1.5), hold on,
plot(time, X_kalman_estimated(:,4),'r','LineWidth',1.5), hold on,
plot(time, X_Huber_estimated(:,4),'b','LineWidth',1.5), hold on,
xlabel('[time] = s'), ylabel('[vx] = km/s'), grid on
subplot(3,1,2), plot(time, relative_motion_LVLH(:,5),'k','LineWidth',1.5), hold on,
plot(time, X_dsdre_estimated(:,5),'c','LineWidth',1.5), hold on,
plot(time, X_kalman_estimated(:,5),'r','LineWidth',1.5), hold on,
plot(time, X_Huber_estimated(:,5),'b','LineWidth',1.5), hold on,
xlabel('[time] = s'), ylabel('[vy] = km/s'), grid on
subplot(3,1,3), plot(time, relative_motion_LVLH(:,6),'k','LineWidth',1.5), hold on,
plot(time, X_dsdre_estimated(:,6),'c','LineWidth',1.5), hold on,
plot(time, X_kalman_estimated(:,6),'r','LineWidth',1.5), hold on,
plot(time, X_Huber_estimated(:,6),'b','LineWidth',1.5), hold on,
xlabel('[time] = s'), ylabel('[vz] = km/s'), grid on

figure,
subplot(3,1,1), plot(time, control_u(:,1)), xlabel('[time] = s'), ylabel('[ux] = km/s^2'), grid on
subplot(3,1,2), plot(time, control_u(:,2)), xlabel('[time] = s'), ylabel('[uy] = km/s^2'), grid on
subplot(3,1,3), plot(time, control_u(:,3)), xlabel('[time] = s'), ylabel('[uz] = km/s^2'), grid on


% relative motion in LVLH frame
figure,
%patch([0 Ci(1,3)*200 Ci(2,3)*200],[0 -Ci(1,4)*200 -Ci(2,4)*200], 'green'), hold on,
% contour(X1,Z1,weight_functions1(X1,Z1,Ci)), hold on,
% contour(X1,Z1,weight_functions2(X1,Z1,Ci)), hold on,
plot3(relative_motion_LVLH(1,1), relative_motion_LVLH(1,2),relative_motion_LVLH(1,3),'ro','LineWidth',2), hold on,
plot3(relative_motion_LVLH(:,1), relative_motion_LVLH(:,2),relative_motion_LVLH(:,3),'r','LineWidth',2), hold on, 
plot3(0 ,0 , 0, 'b*','LineWidth',3), title('Relative motion in LVLH'), xlim([-50 50]), ylim([-50 50])
ax = gca;
ax.XAxisLocation = 'origin';
ax.YAxisLocation = 'origin';
ax.YLim = [-1.2 0.3];
ax.XLim = [-0.5 0.5];
xlabel('[x] = km'), ylabel('[y] = km'), zlabel('[z] = km'),
grid on

% relative motion in ECI frame
figure, 
plot3(relative_motion(1,1), relative_motion(1,2),relative_motion(1,3),'go','LineWidth',2), hold on,
plot3(relative_motion(:,1), relative_motion(:,2),relative_motion(:,3),'g','LineWidth',2),
hold on, plot3(0 ,0 , 0, 'b*','LineWidth',3), title('Relative motion in ECI')
% ax = gca;
% ax.XAxis.Direction = 'reverse';
% ax.YAxis.Direction = 'reverse';
% ax.ZAxis.Direction = 'reverse';
xlabel('[x] = km'), ylabel('[y] = km'), zlabel('[z] = km'),
grid on

% motion in ECI
earth_plot, hold on,
% [xx, yy, zz] = sphere(100);
% surf(rL*xx, rL*yy, rL*zz), hold on,
plot3(relative_motion_t(:,1), relative_motion_t(:,2),relative_motion_t(:,3),'k','LineWidth',2), hold on,
%plot3(relative_motion_c(:,1), relative_motion_c(:,2),relative_motion_c(:,3),'r','LineWidth',2), hold on,
plot3(relative_motion_t(1,1), relative_motion_t(1,2),relative_motion_t(1,3),'ko','LineWidth',2), hold on,
%plot3(relative_motion_c(1,1), relative_motion_c(1,2),relative_motion_c(1,3),'ro','LineWidth',2), title('Motion in ECI'),
% ax = gca;
% ax.XAxis.Direction = 'reverse';
% ax.YAxis.Direction = 'reverse';
% ax.ZAxis.Direction = 'reverse';
xlabel('[x] = km'), ylabel('[y] = km'), zlabel('[z] = km'),
grid on