% Valutating errors

int_err = @integral_err;
pl_error = @plot_err;
mkdir([folder '/errors'])

% Index error computation
C_tab_err = int_err(time, relative_motion_LVLH - X_estimated, 'Cartesian', choose_filter, folder);
S_tab_err = int_err(time, S_relative_motion_LVLH - S_estimated, 'Spherical', choose_filter, folder);

pl_error(time, relative_motion_LVLH - X_estimated , choose_filter, 'Cartesian', folder); 
pl_error(time, S_relative_motion_LVLH - S_estimated , choose_filter, 'Spherical', folder);


%% Utility functions 
function [tbl] = integral_err(t, e, coordinates, filter, folder)

% choose the correct frame
if(strcmp(coordinates, 'Cartesian') == 1)
    state = {'x [km]'; 'y [km]'; 'z [km]'; 'vx [km/s]';'vy [km/s]';'vz [km/s]'};
    rd = 1;         % from rad to deg
elseif(strcmp(coordinates, 'Spherical') == 1)
    state = {'\rho [km]'; '\theta [deg]'; '\Phi [deg]'; '\rho^{\prime} [km/s]'; '\theta^{\prime} [deg/s]'; '\Phi^{\prime} [deg/s]'};
    rd = 180/pi;     % from rad to deg
end

% choose the correct filter
switch(filter)
    case 1
        filt = 'DSDRE';
    case 2
        filt = 'EKF';
    case 3
        filt = 'Huber';
end

% ISE index

ex_ise = trapz(t,abs(e(:,1)));

ey_ise = trapz(t,abs(rd*e(:,2)));

ez_ise = trapz(t,abs(rd*e(:,3)));

evx_ise = trapz(t,abs(e(:,4)));

evy_ise = trapz(t,abs(rd*e(:,5)));

evz_ise = trapz(t,abs(rd*e(:,6)));


% ITSE index 

ex_itse = trapz(t, t.*abs(e(:,1)));

ey_itse = trapz(t, t.*abs(rd*e(:,2)));

ez_itse = trapz(t, t.*abs(rd*e(:,3)));

evx_itse = trapz(t, t.*abs(e(:,4)));

evy_itse = trapz(t, t.*abs(rd*e(:,5)));

evz_itse = trapz(t, t.*abs(rd*e(:,6)));


% output
e_ise = [ex_ise ey_ise ez_ise evx_ise evy_ise evz_ise]';
e_itse = [ex_itse ey_itse ez_itse evx_itse evy_itse evz_itse]';

mkdir([folder '/errors' '/' filt])

tbl = table(state, e_ise, e_itse, 'VariableNames', {'state';'IAE'; 'ITAE'});
save([folder '/errors' '/' filt '/error_index_tab.mat'], 'tbl');
end


function plot_err(t, e, filt, coordinates, folder)

cart = [string('x [km]'), string('y [km]'),string('z [km]'),string('v_x [km/s]'), string('v_y [km/s]'), string('v_z [km/s]')];
sphe = [string('\rho [km]'), string('\theta [deg]'), string('\Phi [deg]'), string('\rho^{\prime} [km/s]'), string('\theta^{\prime} [deg/s]'), string('\Phi^{\prime} [deg/s]')];

if(strcmp(coordinates, 'Cartesian') == 1)
    coord = cart;
    rd = 1;         % from rad to deg
elseif(strcmp(coordinates, 'Spherical') == 1)
    coord = sphe;
    rd = 180/pi;    % from rad to deg
end

if(filt == 1)
    col = 'c';
    filter = 'DSDRE';
elseif(filt == 2)
    col = 'r';
    filter = 'EKF';
elseif(filt == 3)
    col = 'b';
    filter = 'Huber';
end


% plot errors in position 
figure, suptitle('Errors')
subplot(3,1,1)
plot(t, e(:,1), col,'LineWidth',1.5),
xlabel('time [s]'), ylabel(coord(1)),legend(['|e| ' filter])

subplot(3,1,2)
plot(t, rd*e(:,2), col,'LineWidth',1.5),
xlabel('time [s]'), ylabel(coord(2)),legend(['|e| ' filter])

subplot(3,1,3)
plot(t, rd*e(:,3), col,'LineWidth',1.5),
xlabel('time [s]'), ylabel(coord(3)),legend(['|e| ' filter])

savefig([folder '/errors/' filter '_' coordinates '_coordianates_position_error.fig'])
saveas(gcf,[folder '/errors/' filter '_' coordinates '_coordianates_position_error.eps'],'epsc')

% plot errors in velocity 
figure, suptitle('Errors')
subplot(3,1,1)
plot(t, e(:,4), col,'LineWidth',1.5),
xlabel('time [s]'), ylabel(coord(4)),legend(['|e| ' filter])

subplot(3,1,2)
plot(t, rd*e(:,5), col,'LineWidth',1.5),
xlabel('time [s]'), ylabel(coord(5)),legend(['|e| ' filter])

subplot(3,1,3)
plot(t, rd*e(:,6), col,'LineWidth',1.5),
xlabel('time [s]'), ylabel(coord(6)),legend(['|e| ' filter])

savefig([folder '/errors' '/' filter '_' coordinates '_coordianates_velocity_error.fig'])
saveas(gcf,[folder '/errors/' filter '_' coordinates '_coordianates_velocity_error.eps'],'epsc')


end
