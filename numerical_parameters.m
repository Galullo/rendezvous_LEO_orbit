%% Numerical parameter for Hill's equations

clear, close all,

%% orbital parameters

rT      = 6.371 * 10^(3);                    % radius of Earth in km;
rL      = 3.476 * 10^(3);                    % radius of Moon in km
r_LEO   = 390 * 10^(0);                             % radaius LEO in km; 390

G       = 6.67408 * 10^(-20);               % Gravitational constant, [km^3 * kg^(-1) * s^(-2)];
M_Earth = 5.972 * 10^(24);                  % Mass of Earth, [kg]

mu      = G*M_Earth;                        % standard gravitational parameter; % 0.0123

rp_e    = 600;                              % km

ra_e    = 60530;                            % Km

%% System parameters

% input matrix for Dynamic System
B0 = [zeros(3,3); eye(3)];

% initial contidion of Dynamics (Cartesian)
X0_ch = [-0.01 -1 0 0 0 0]';

%% filter

% sampling time of filter
Ts = 1;

% sampling time of sensor
Tsens = 1;

% matrix of output
Hsens = [eye(3), zeros(3,3)];

% standard deviotion in initial condition;
std_i_rho       = 0.01;         % km
std_i_theta     = 0.1;          % deg
std_i_phi       = 0.1;          % deg
std_i_dot_rho   = 1 * 10^(-4);  % km/s
std_i_dot_theta = 0.01;         % deg/s
std_i_dot_phi   = 0.01;         % deg/s

% estimate init cond in filters
delta_S0_ch_f = [normrnd(0, std_i_rho), pi/180*normrnd(0, std_i_theta), pi/180*normrnd(0, std_i_phi),...
    normrnd(0, std_i_dot_rho), pi/180*normrnd(0, std_i_dot_theta), pi/180*normrnd(0, std_i_dot_phi)]';

% initial condition of Filters 
S0_ch_f = cartesian2spherical(X0_ch) + delta_S0_ch_f;   % Spherical Frame
X0_ch_f = spherical2cartesian(S0_ch_f);                 % Cartesian Frame

% matrix for input process noise
GAMMA = eye(6);

Qf = diag(([0 0 0 10^(-6) 10^(-6) 10^(-6)] * 10^(0)).^2);

Rf = diag([1.518*10^(-5), 2.787*(pi/180)*10^(-3), 1.404*(pi/180)*10^(-3)].^2)*10^(0);

Pf = diag([0.01, 0.1*pi/180, 0.01*pi/180, 10^(-4), 0.01*pi/180, 0.01*pi/180].^2)*10^(0);


%% Controller parameters

Q0 = [7*10^(-4)*eye(3), zeros(3,3); zeros(3,3), 3*10^(2)*eye(3)];
R0 = 4*10^(5)*eye(3);  

% Q0 = [7*10^(-4)*eye(3), zeros(3,3); zeros(3,3), 3*10^(2)*eye(3)];  
% R0 = 4*10^(5)*eye(3);                                              


%% Simulation

% Runge-Kutta
Trk = 0.01;

% time for simulink simulation
t_sim = 100; %3600;                       % seconds
