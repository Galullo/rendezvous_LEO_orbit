function [dx_dy] = jacobian_spher2cart(Y)
% jacobian from cartesian to spherical coordiantes

rho         = Y(1);
theta       = Y(2);
phi         = Y(3);
rho_dot     = Y(4);
theta_dot   = Y(5);
phi_dot     = Y(6);





dx_dy_p = [cos(phi)*cos(theta), -rho*cos(phi)*sin(theta), -rho*cos(theta)*sin(phi), 0, 0, 0;
        cos(phi)*sin(theta), rho*cos(phi)*cos(theta), -rho*sin(phi)*sin(theta), 0, 0, 0;
        sin(phi), 0, rho*cos(phi), 0, 0, 0;
        - phi_dot*cos(theta)*sin(phi) - theta_dot*cos(phi)*sin(theta), phi_dot*rho*sin(phi)*sin(theta) - rho_dot*cos(phi)*sin(theta) - rho*theta_dot*cos(phi)*cos(theta),...
        rho*theta_dot*sin(phi)*sin(theta) - rho_dot*cos(theta)*sin(phi) - phi_dot*rho*cos(phi)*cos(theta), cos(phi)*cos(theta), -rho*cos(phi)*sin(theta), -rho*cos(theta)*sin(phi);
        theta_dot*cos(phi)*cos(theta) - phi_dot*sin(phi)*sin(theta), rho_dot*cos(phi)*cos(theta) - rho*theta_dot*cos(phi)*sin(theta) - phi_dot*rho*cos(theta)*sin(phi),...
        - rho_dot*sin(phi)*sin(theta) - rho*theta_dot*cos(theta)*sin(phi) - phi_dot*rho*cos(phi)*sin(theta), cos(phi)*sin(theta), rho*cos(phi)*cos(theta), -rho*sin(phi)*sin(theta);
        phi_dot*cos(phi), 0, rho_dot*cos(phi) - phi_dot*rho*sin(phi), sin(phi), 0, rho*cos(phi)];


% for i = 1:6
%     dx_dy_p(i,:) = dx_dy_p(i,:)/norm(dx_dy_p(i,:));
% end

dx_dy = dx_dy_p;

end