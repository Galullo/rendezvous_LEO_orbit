% Comparison filter

time_s = 0:Tsens:t_sim;

%pl_state = @plot_states;
pl_cl_state = @plot_cl_states;

% choose orbit:
%             - 1 Low Earth Orbit;
%             - 2 Proba-3 Orbit;

for choose_orbit = 1:1
    
    switch(choose_orbit)
        case 1
            orbit_name = 'Low_Earth_Orbit';
            %...Input data LEO:
            %  Prescribed initial orbital parameters of target A:
            rp    = rT + r_LEO;         % km
            e     = 0.0005100;
            i     = 51.6391;            % deg
            RA    = 270.8069;           % deg
            omega = 116.0974;           % deg
            theta = 0;
            
            %  Additional computed parameters:
            ra = rp*(1 + e)/(1 - e);
            h  = sqrt(2*mu*rp*ra/(ra + rp));   %(km^2/)s
            a  = (rp + ra)/2;
            T  = 2*pi/sqrt(mu)*a^1.5;
            n  = 2*pi/T;
            
            % Error's model
            distr_error;
        case 2
            orbit_name = 'Proba3_Orbit';
            %...Input data Orbit Molnija:
            %  Prescribed initial orbital parameters of target A:
            rp    =  rT + rp_e;             % km
            ra    =  rT + ra_e;             % km
            i     =  59;                    % deg
            RA    =  84;                    % deg
            omega =  188;                   % deg
            theta = 0;                      % deg
            
            %  Additional computed parameters:
            e = (ra - rp)/(rp + ra);
            h  = sqrt(2*mu*rp*ra/(ra + rp));   %(km^2/)s
            a  = (rp + ra)/2;
            T  = 2*pi/sqrt(mu)*a^1.5;
            n  = 2*pi/T;
     
            % Error's model
            distr_error;
    end
    
% choose orbit:
%             - 1 DSDER filter;
%             - 2 EKF filter;
%             - 3 Huber filter;
%             - 4 without filter;

    
    for choose_filter = 1:1
        
        sim('complete_guidance_navigation', t_sim)
        warning('off')
        
        % conversion of relative motion in spherical coordiantes
        S_relative_motion_LVLH = zeros(length(relative_motion_LVLH), 6);
        for i = 1:length(relative_motion_LVLH)
            S_relative_motion_LVLH(i,:) = cartesian2spherical(relative_motion_LVLH(i,:))';
        end
        
        S_estimated = zeros(length(X_estimated), 6);
        for i = 1:length(X_estimated)
            S_estimated(i,:) = cartesian2spherical(X_estimated(i,:))';
        end
        
        
        switch(choose_filter)
            case 1
                folder = ['prova_funziona/cl/' orbit_name  '/DSDRE'] ;
                mkdir(folder)
                X_dsdre_estimated = X_estimated;
                S_dsdre_estimated = S_estimated;
                save([folder '/X_dsdre_estimated.mat'], 'X_dsdre_estimated');
                save([folder '/S_dsdre_estimated.mat'], 'S_dsdre_estimated');
                save([folder '/relative_motion_LVLH.mat'], 'relative_motion_LVLH','S_relative_motion_LVLH');
                
            case 2
                folder = ['prova_funziona/cl/' orbit_name  '/EKF'];
                mkdir(folder)
                X_kalman_estimated = X_estimated;
                S_kalman_estimated = S_estimated;
                save([folder '/X_kalman_estimated.mat'], 'X_kalman_estimated');
                save([folder '/S_kalman_estimated.mat'], 'S_kalman_estimated');
                save([folder '/relative_motion_LVLH.mat'], 'relative_motion_LVLH','S_relative_motion_LVLH');
                
            case 3
                folder = ['prova_funziona/cl/' orbit_name  '/HUBER'];
                mkdir(folder)
                X_Huber_estimated = X_estimated;
                S_Huber_estimated = S_estimated;
                save([folder '/X_Huber_estimated.mat'], 'X_Huber_estimated');
                save([folder '/S_Huber_estimated.mat'], 'S_Huber_estimated');
                save([folder '/relative_motion_LVLH.mat'], 'relative_motion_LVLH','S_relative_motion_LVLH');
            case 4
                folder = ['prova_funziona/cl/' orbit_name];
                mkdir(folder)
                save([folder '/relative_motion_LVLH.mat'], 'relative_motion_LVLH','S_relative_motion_LVLH');
        end

        if(choose_filter ~= 4)
            comparison_error_new;
            mkdir([folder '/figures'])
            pl_cl_state(time, time_s, measurements, X_estimated, relative_motion_LVLH, choose_filter, 'Cartesian', folder);
            pl_cl_state(time, time_s, S_measurements, S_estimated, S_relative_motion_LVLH, choose_filter, 'Spherical', folder);
        end
        
        close all
    end
    
    save([folder '/init_cond.mat'], 'X0_ch_f', 'X0_ch', 'S0_ch_f', 'delta_S0_ch_f')
end


%% Utility functions 



function plot_cl_states(t, ts, s_sens, estimated, tr_st, filt, coordinates, folder)

cart = [string('x [km]'), string('y [km]'),string('z [km]'),string('v_x [km/s]'), string('v_y [km/s]'), string('v_z [km/s]')];
sphe = [string('\rho [km]'), string('\theta [deg]'), string('\Phi [deg]'), string('\rho^{\prime} [km/s]'), string('\theta^{\prime} [deg/s]'), string('\Phi^{\prime} [deg/s]')];

if(strcmp(coordinates, 'Cartesian') == 1)
    coord = cart;
    rd = 1;         % from rad to deg
elseif(strcmp(coordinates, 'Spherical') == 1)
    coord = sphe;
    rd = 180/pi;    % from rad to deg
end

switch(filt)
    case 1
        col = 'c';
        filter = 'DSDRE';
    case 2
        col = 'r';
        filter = 'EKF';
    case 3
        col = 'b';
        filter = 'Huber';
end
        

figure,
subplot(3,1,1)
plot(ts, s_sens(:,1), 'k.', 'MarkerSize', 12), hold on,
plot(t, estimated(:,1), col,'LineWidth', 1.5), hold on,
xlabel('time [s]'), ylabel(coord(1)), legend('measurements', filter)

subplot(3,1,2)
plot(ts, rd*s_sens(:,2), 'k.', 'MarkerSize', 12), hold on,
plot(t, rd*estimated(:,2), col, 'LineWidth',1.5),hold on,
xlabel('time [s]'), ylabel(coord(2)), legend('measurements', filter)

subplot(3,1,3)
plot(ts, rd*s_sens(:,3), 'k.', 'MarkerSize',12), hold on,
plot(t, rd*estimated(:,3), col, 'LineWidth',1.5),hold on,
xlabel('time [s]'), ylabel(coord(3)), legend('measurements', filter)

savefig([folder '/figures/' coordinates '_coordianates_position.fig'])
saveas(gcf,[folder '/figures/' coordinates '_coordianates_position.eps'],'epsc')

% plot velocity
figure,
subplot(3,1,1)
plot(t, tr_st(:,4), 'k', 'LineWidth',1.5),hold on,
plot(t, estimated(:,4), col, 'LineWidth',1.5),hold on,
xlabel('time [s]'), ylabel(coord(4)), legend('real', filter)

subplot(3,1,2)
plot(t, rd*tr_st(:,5), 'k', 'LineWidth',1.5),hold on,
plot(t, rd*estimated(:,5), col, 'LineWidth',1.5),hold on,
xlabel('time [s]'), ylabel(coord(5)), legend('real', filter)

subplot(3,1,3)
plot(t, rd*tr_st(:,6), 'k', 'LineWidth', 1.5), hold on,
plot(t, rd*estimated(:,6), col, 'LineWidth', 1.5), hold on,
xlabel('time [s]'), ylabel(coord(6)), legend('real', filter)

savefig([folder '/figures/' coordinates '_coordianates_velocity.fig'])
saveas(gcf,[folder '/figures/' coordinates '_coordianates_velocity.eps'],'epsc')
end





%     % Estimation comparison plot
%     load([folder '/DSDRE/X_dsdre_estimated.mat'])
%     load([folder '/EKF/X_kalman_estimated.mat'])
%     load([folder '/HUBER/X_Huber_estimated.mat'])
%     load([folder '/relative_motion_LVLH.mat'])
%     pl_state(time, time_s, measurements, X_dsdre_estimated, X_kalman_estimated, X_Huber_estimated, relative_motion_LVLH, 'Cartesian', folder)
%     
%     pl_state(time, time_s, S_measurements, S_dsdre_estimated, S_kalman_estimated, S_Huber_estimated, S_relative_motion_LVLH, 'Spherical', folder)

